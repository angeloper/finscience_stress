from locust import HttpLocust, TaskSet, task
import json

class UserBehavior(TaskSet):

    def __init__(self, parent):
        super(UserBehavior, self).__init__(parent)

        self.token = ""
        self.headers = {}

        import resource
        resource.setrlimit(resource.RLIMIT_NOFILE, (10240, 9223372036854775807))

    def on_start(self):
        #self.token = self.login()
        self.token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJmaW5zY2llbmNlLmNvbSIsInN1YiI6MjMsImV4cCI6MTU1ODE4NzEyOTMzNiwiaWF0IjoxNTU4MTAwNzI5fQ.9dAk_zAyLwaRE1PDMa6-cyXCSuXY8g0rjYoQUWInaRAtlhzLyP_amh-UlNgoRonJNeS-VIaQwuIea3gOwV47cw"

        self.headers = {'Authorization': 'Bearer ' + self.token}

    # def login(self):
    #     response = self.client.post("/user/login", data={"email":"angelo.perrone@finscience.com", "password":"****"})
    #     print(response)
    #     return json.loads(response._content)['token']

    # @task(2)
    # def index(self):
    #     self.client.get("/#/", headers=self.headers)

    @task(1)
    def profile(self):
        response = self.client.get("/portfolios", headers=self.headers)

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    max_wait = 10000