const gulp = require('gulp');
const rsync = require('gulp-rsync');
const GulpSSH = require('gulp-ssh');
const fs = require('fs');
const os = require('os');
const gulpsync = require('gulp-sync')(gulp);
const bump = require('gulp-bump');

const staging = '35.187.36.85';
const stress_folder = '/home/finscience/stress';

var stress_gulpSSH = new GulpSSH({
    ignoreErrors: false,
    sshConfig: {
        host: staging,
        port: 22,
        username: 'finscience',
        privateKey: fs.readFileSync(os.homedir() + '/.ssh/id_rsa')
    }
});

// Versioning management
// -----------------------------------------------------------------------------
// major release
gulp.task('bump-major', function(){
    gulp.src(['./package.json'])
    .pipe(bump({type:'major'}))
    .pipe(gulp.dest('./'));
});

// minor release
gulp.task('bump-minor', function(){
    gulp.src(['./package.json'])
    .pipe(bump({type:'minor'}))
    .pipe(gulp.dest('./'));
});

// patch release
gulp.task('bump-patch', function(){
    gulp.src(['./package.json'])
    .pipe(bump({type:'patch'}))
    .pipe(gulp.dest('./'));
});

// tag the git repository with this version number
gulp.task('version', function(){    
    setTimeout(function(){
        gulp.src(['./package.json']).pipe(tag_version());
    }, 1000)    
});


// Build
// -----------------------------------------------------------------------------
gulp.task('build-major', gulpsync.sync([
    'bump-major',
    'version',
    []
]));

gulp.task('build-minor', gulpsync.sync([
    'bump-minor',
    'version',
    []
]));

gulp.task('build-patch', gulpsync.sync([
    'bump-patch',
    'version',
    []
]));

gulp.task('build', function(){
    console.log("Please use one of the following tasks:\n" +
        "gulp build-patch\n" + 
        "gulp build-minor\n" + 
        "gulp build-major");
})

// Perform npm install and restart the server on the remote machine
// -----------------------------------------------------------------------------
gulp.task('start-staging_intelligence-server', function () {
  return stress_gulpSSH
    .shell([
        'cd ' + dist_folder,
        'npm install --production',
        'pm2 stop finscience_server.js',
        'npm run start:staging_intelligence'
    ], {filePath: 'deploy.log'})
    .pipe(gulp.dest('.'))
});

// Deploy to staging server
// -----------------------------------------------------------------------------
gulp.task('deploy-to-staging', function () {
    return gulp.src('./')
    .pipe(rsync({
        root: './',
        hostname: staging,
        username: 'finscience',
        destination: stress_folder,
        recursive: true,
        clean: true,
        exclude: ['.DS_Store', 'deploy.log', '.git', '.gitignore', 'gulpfile.js', 'README.md', 'node_modules', 'coverage', 'test', 'finscience.sql']
    }));
});

gulp.task('deploy-staging', gulpsync.sync([
    'deploy-to-staging',
    //'start-staging_intelligence-server',
    []
]));